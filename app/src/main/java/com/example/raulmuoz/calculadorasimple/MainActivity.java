package com.example.raulmuoz.calculadorasimple;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Button;
import android.support.v7.app.AppCompatActivity;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    private boolean contienePunto=false;
    private String currentOperation="";
    private double operacion=0;
    private boolean op1active=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View.OnClickListener listener=new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b=(Button)v;  //pressed button
                String c=b.getText().toString();  //text from pressed button
                TextView d=findViewById(R.id.txtResultado); //"standard" display
                TextView s=findViewById(R.id.opTextView);   //"sign" display
                //perform activity
                switch (c) {
                    case "C":
                        d.setText("");
                        break;
                    case "AC":
                        currentOperation="";
                        op1active=false;
                        operacion=0;
                        d.setText("");
                        s.setText("");
                        break;
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                    case "4":
                    case "5":
                    case "6":
                    case "7":
                    case "8":
                    case "9":
                        d.setText(d.getText()+c);
                        break;
                    case ".":
                        if (!contienePunto) {
                            d.setText(d.getText()+".");
                            contienePunto=true;
                        }
                        break;
                    case "+":
                    case "-":
                    case "x":
                    case "/":
                        s.setText(currentOperation=c);
                        operacion=Double.parseDouble(d.getText().toString());
                        op1active=true;
                        d.setText("");
                        contienePunto=false;
                        break;
                    case "=":
                        if (op1active) {
                            switch (currentOperation) {
                                case "+":
                                    operacion += Double.parseDouble(d.getText().toString());
                                    break;
                                case "-":
                                    operacion -= Double.parseDouble(d.getText().toString());
                                    break;
                                case "x":
                                    operacion *= Double.parseDouble(d.getText().toString());
                                    break;
                                case "/":
                                    operacion /= Double.parseDouble(d.getText().toString());
                                    break;
                            }
                            s.setText(currentOperation="");
                            op1active=false;
                            contienePunto=false;
                            d.setText(Double.toString(operacion));
                        }
                }
            }
        };

        Button b0=findViewById(R.id.btn0);
        Button b1=findViewById(R.id.btn1);
        Button b2=findViewById(R.id.btn2);
        Button b3=findViewById(R.id.btn3);
        Button b4=findViewById(R.id.btn4);
        Button b5=findViewById(R.id.btn5);
        Button b6=findViewById(R.id.btn6);
        Button b7=findViewById(R.id.btn7);
        Button b8=findViewById(R.id.btn8);
        Button b9=findViewById(R.id.btn9);
        Button bDot=findViewById(R.id.btnPunto);
        Button bPlus=findViewById(R.id.btnSuma);
        Button bMinus=findViewById(R.id.btnResta);
        Button bTimes=findViewById(R.id.btnMultiplicacion);
        Button bDivide=findViewById(R.id.btnDivision);
        Button bC=findViewById(R.id.btnC);
        Button bCE=findViewById(R.id.btnAC);
        Button bPc=findViewById(R.id.btnModulo);
        Button bEq=findViewById(R.id.btnIgual);


        b0.setOnClickListener(listener);
        b1.setOnClickListener(listener);
        b2.setOnClickListener(listener);
        b3.setOnClickListener(listener);
        b4.setOnClickListener(listener);
        b5.setOnClickListener(listener);
        b6.setOnClickListener(listener);
        b7.setOnClickListener(listener);
        b8.setOnClickListener(listener);
        b9.setOnClickListener(listener);
        bDot.setOnClickListener(listener);
        bPlus.setOnClickListener(listener);
        bMinus.setOnClickListener(listener);
        bTimes.setOnClickListener(listener);
        bDivide.setOnClickListener(listener);
        bC.setOnClickListener(listener);
        bCE.setOnClickListener(listener);
        bPc.setOnClickListener(listener);
        bEq.setOnClickListener(listener);
    }
}
